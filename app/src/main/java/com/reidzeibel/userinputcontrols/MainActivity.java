package com.reidzeibel.userinputcontrols;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    // Definisikan variabel
    Spinner spinner;
    ToggleButton toggleButton;
    Switch switchButton;
    RadioGroup radioGroup;
    CheckBox checkBox1, checkBox2, checkBox3;
    Button kirimRadio, kirimCheckbox, kirimSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Inisialisasi Adapter untuk Spinner
        ArrayAdapter<CharSequence> adapter =
                ArrayAdapter.createFromResource(this, R.array.pilihan, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Inisialisasi Spinner
        spinner = findViewById(R.id.spinner);
        spinner.setAdapter(adapter); // set spinner disini
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            // Untuk merespon ketika item pada spinner dipilih
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                showToast("Pilihan spinner sekarang : " + getResources().getStringArray(R.array.pilihan)[position]);
            }

            // Untuk merespon jika tidak ada item pada spinner yang dipilih
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // inisialisasi Radio Group
        radioGroup = findViewById(R.id.radioGroup);

        // set apa yang terjadi jika sebuah radio button pada radio group dipilih
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.pilihanA :
                        showToast("Pilihan sekarang : A");
                        break;
                    case R.id.pilihanB :
                        showToast("Pilihan sekarang : B");
                        break;
                    case R.id.pilihanC :
                        showToast("Pilihan sekarang : C");
                        break;
                }
            }
        });

        // inisialisasi tombol "Kirim Radio"
        kirimRadio = findViewById(R.id.submit_radio);
        kirimRadio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (radioGroup.getCheckedRadioButtonId()) {
                    case R.id.pilihanA :
                        showToast("RadioButton A");
                        break;
                    case R.id.pilihanB :
                        showToast("RadioButton B");
                        break;
                    case R.id.pilihanC :
                        showToast("RadioButton C");
                        break;
                }
            }
        });

        // inisialisasi checkbox dan tombol kirim checkbox
        kirimCheckbox = findViewById(R.id.submit_checkbox);
        checkBox1 = findViewById(R.id.opsi1);
        checkBox2 = findViewById(R.id.opsi2);
        checkBox3 = findViewById(R.id.opsi3);

        kirimCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = "";
                if (checkBox1.isChecked()) {
                    text = text.concat("Opsi 1 ");
                }
                if (checkBox2.isChecked()) {
                    text = text.concat("Opsi 2 ");
                }
                if (checkBox3.isChecked()) {
                    text = text.concat("Opsi 3 ");
                }

                showToast("Yang dipilih adalah " + text);
            }
        });

        // inisialisasi toggleButton
        toggleButton = findViewById(R.id.toggle);

        // Untuk merespon perubahan pada toggleButton (on/off)
        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) { // kalau isChecked = true, berarti kondisi ON, jika false maka OFF
                    showToast("Toggle Dinyalakan");
                } else {
                    showToast("Toggle Dimatikan");
                }
            }
        });

        // inisialisasi Switch
        switchButton = findViewById(R.id.switchView);

        // Untuk merespon perubahan pada Switch (on/off)
        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) { // kalau isChecked = true, berarti kondisi ON, jika false maka OFF
                    showToast("Switch Dinyalakan");
                } else {
                    showToast("Switch Dimatikan");
                }
            }
        });

        // inisialisasi tombol "Kirim Spinner"
        kirimSpinner = findViewById(R.id.kirimSpinner);
        kirimSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToast("Spinner dipilih adalah : " + getResources().getStringArray(R.array.pilihan)[spinner.getSelectedItemPosition()]);
            }
        });
    }

    // untuk menampilkan toast message
    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

}